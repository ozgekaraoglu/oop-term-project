#ifndef MOVIE_H
#define MOVIE_H
#include<string>
#include<iostream>
#include "Date.h"
using namespace std;

class Movie{
	public:
		enum Status {  Out, Discontinued, Cancelled  };	
		enum Genres { Suspense, Foreign, Comedy, Western, Music, Drama, Other };
		static Status statusOf(string s);
		static Genres genreOf(string g);
	private:
		//member variables
		string _DVD_Title; 
		Status _status;
		double _Price;
		int _Year;
		Genres _genre;
		string _genreName;
		string _ID;
		Date*_DVD_ReleaseDate;
		
	public:
		Movie(string dvdTitle,string status, string Price,string Year, string genre, Date* DVD_ReleaseDate, string ID);
		~Movie();
		//getters&setters
		string getDVD_Title() const;
		Movie* setDVD_Title(string);
		Status getStatus() const;
		Movie* setStatus(Status);
		double getPrice() const;
		Movie* setPrice(double);
		int getYear() const;
		Movie* setYear(int);
		Genres  getGenre() const;
		string getGenreName() const;
		Movie* setGenreName(string genreName);
		Movie* setGenre(Genres);
		string getID() const;
		Movie* setID(string);
		Date* getDVD_ReleaseDate() const;
		Movie* setDVD_ReleaseDate(Date* date);
        bool operator>(Movie* movie);
        bool operator==(Movie* movie);
        friend ostream& operator<<(ostream& os, const Movie& dt);
};



#endif