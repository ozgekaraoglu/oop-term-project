//
//  AfterDate.hpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/3/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#ifndef AfterDate_hpp
#define AfterDate_hpp

#include <stdio.h>
#include "Predicate.h"
#include "Movie.h"
class AfterDate : public Predicate {
public:
    AfterDate(Date* date);
    virtual bool test(Movie* movie);
    ~AfterDate();
    
private:
    Date *_date;
};
#endif /* AfterDate_hpp */
