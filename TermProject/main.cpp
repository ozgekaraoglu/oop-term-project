#include<iostream>
#include<string>
#include<cstdlib>
#include<fstream>
#include "ViewController.h"
#include "FileLineTraverser.h"
#include "MovieDecoder.h"
using namespace std;


int main(){
	FileLineTraverser traverser("/Users/sercan_karaoglu/git/ozge_odev/oop-term-project/TermProject/DVD_list.txt");
	MovieDecoder *decoder=new MovieDecoder();
	traverser.traverseUsing(decoder);
	vector<Movie*> movies = decoder->getMovies();	

	MovieController* controller = new MovieController(movies.begin(),movies.end());
    
    ViewController *vC = new ViewController(controller);
    vC->displayMenu();
	
	delete decoder;
    delete vC;
    delete controller;
    movies.clear();
	system("PAUSE");
	return 0;
}