#include "MovieController.h"


MovieController::MovieController(MovieConsumer* consumer,MovieController* head,vector<Movie*>::iterator begin, vector<Movie*>::iterator end) : downstream(consumer), _head(head),_begin(begin),_end(end){}
MovieController::MovieController(vector<Movie*>::iterator begin, vector<Movie*>::iterator end): _head(this),_begin(begin),_end(end){}

MovieController::~MovieController(void){}
MovieController* MovieController::filter(Predicate* predicate){
	//anonymous class
	class Filter: public MovieConsumer {
	public:
		Filter(Predicate* p,MovieController* d): _p(p), _d(d) {}
		virtual void begin(){
			_d->downstream->begin();
		}
		virtual void accept(Movie* movie){
			if(_p->test(movie)){
				_d->downstream->accept(movie);
			}
		}
		virtual void end(){
			_d->downstream->end();
		}
	private:
		Predicate* _p;
		MovieController* _d;
		
	};
	return new MovieController(new Filter(predicate, this),_head,_begin,_end);
}
Movie* MovieController::findFirst(){
    class FindFirst: public MovieConsumer {
    public:
        FindFirst() {}
        virtual void begin(){
        }
        virtual void accept(Movie* movie){
            this->_movie = movie;
        }
        virtual void end(){
        }
        Movie* getMovie(){
            return _movie;
        }
    private:
        Movie* _movie=nullptr;
    };
    FindFirst *findFirst = new FindFirst();
    
    _head->downstream = findFirst;
    
    downstream->begin();
    for(vector<Movie*>::iterator it = _begin ; it != _end && findFirst->getMovie() == nullptr; ++it){
        downstream->accept(*it);
    }
    downstream->end();
    return findFirst->getMovie();
}

double MovieController::average(){
	class Average: public MovieConsumer {
	public:
		Average() {}
		virtual void begin(){
			size=0;
			total=0;
		}
		virtual void accept(Movie* movie){
			total += movie->getPrice();
			size +=1;
		}
		virtual void end(){
			avg = total /(double)size;
		}
		double getAvg(){
			return avg;
		}
	private:
		int size;
		double total;
		double avg;
	};
    Average *finalizer = new Average();
    this->forEach(finalizer);
    return finalizer->getAvg();
}
int MovieController::count(){
	class Total: public MovieConsumer {
	public:
		Total() {}
		virtual void begin(){
			size=0;
		}
		virtual void accept(Movie* movie){
			size +=1;
		}
		virtual void end(){
			
		}
		double getSize(){
			return size;
		}
	private:
		int size;
	};
    Total *finalizer = new Total();
    this->forEach(finalizer);
    return finalizer->getSize();
}
void MovieController::forEach(MovieConsumer* consumer){
    _head->downstream = consumer;
    
    downstream->begin();
    for(vector<Movie*>::iterator it = _begin; it != _end; ++it){
        downstream->accept(*it);
    }
    downstream->end();
}