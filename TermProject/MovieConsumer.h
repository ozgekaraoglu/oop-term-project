#pragma once
#include "Movie.h"
class MovieConsumer
{
public:
	virtual void begin() = 0;
	virtual void accept(Movie* movie) = 0;
	virtual void end() = 0;
};

