//
//  ByDVDTitle.cpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/4/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#include "ByDVDTitle.hpp"
#include "Util.h"
ByDVDTitle::ByDVDTitle(string title) : _DVDTitle(title){}

Predicate* ByDVDTitle::of(string title){
    if (title.find("*") != std::string::npos){
        class ByDVDTitleWithStar : public  Predicate{
        public:
            ByDVDTitleWithStar(string title) : _DVDTitle(Util::replaceStar(title)){}
            ~ByDVDTitleWithStar(){}
            
            virtual bool test(Movie* movie){
                return movie->getDVD_Title().compare(0,_DVDTitle.length(),_DVDTitle)==0;
            }
        private:
            string _DVDTitle;
            
        };
        return new ByDVDTitleWithStar(title);
    }else {
        return new ByDVDTitle(title);
    }

}
bool ByDVDTitle::test(Movie* movie){
    return movie->getDVD_Title()==_DVDTitle;
}

ByDVDTitle::~ByDVDTitle(){}