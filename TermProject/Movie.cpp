#include"Movie.h"
#include<iostream>
#include<string>
using namespace std;

Movie::Movie(string dvdTitle,string status, string price,string year, string genre, Date* dvdRelease, string ID)
{
	setDVD_Title(dvdTitle);
    setStatus(Movie::statusOf(status));
	string::size_type sz;
	price.replace(price.find("$"), sizeof("$")-1, "");
	setPrice(stod(price,&sz));
	if(year == "UNK"){
		setYear(-2);
	}else if(year == "VAR" ){
		setYear(-1);
	}else{
		setYear(stoi(year));
	}
	setGenreName(genre);
    setGenre(Movie::genreOf(genre));
	setID(ID);
	setDVD_ReleaseDate(dvdRelease);
}

Movie* Movie::setDVD_Title(string d){
	this->_DVD_Title = d;
	return this;
}
string Movie::getDVD_Title() const {
	return this->_DVD_Title;
}

Movie::Status Movie::getStatus() const{
	return this->_status;
}
Movie* Movie::setStatus(Status s){
	this->_status = s;
	return this;
}

double Movie::getPrice() const{
	return _Price;
}

Movie* Movie::setPrice(double p){
	this->_Price = p;
	return this;
}

int Movie::getYear() const{
	return _Year;
}
	
Movie* Movie::setYear(int y){
	this->_Year = y;
	return this;
}

Movie::Genres Movie::getGenre() const{
	return _genre;
}

Movie* Movie::setGenre(Genres g){
	this->_genre = g;
	return this;
}
string Movie::getGenreName() const{
	return this->_genreName;
}
Movie* Movie::setGenreName(string genreName){
	this->_genreName = genreName;
	return this;
}
string Movie::getID() const{
	return _ID;
}

Movie* Movie::setID(string id){
	this->_ID = id;
	return this;
}

Date* Movie::getDVD_ReleaseDate() const{
	return _DVD_ReleaseDate;
}

Movie* Movie::setDVD_ReleaseDate(Date* d){
	this->_DVD_ReleaseDate = d;
	return this;
}
bool Movie::operator>(Movie* movie){
    return this->_Price > movie->getPrice();
}
bool Movie::operator==(Movie* movie){
    return this->_Price == movie->getPrice();
}
ostream& operator<<(ostream& os, const Movie& movie){
    os << movie.getDVD_Title() << " {Genre: " << movie.getGenreName() << ", Price: " << movie.getPrice() << " }";
    return os;
}
Movie::~Movie(void){
	delete _DVD_ReleaseDate;
}
Movie::Status Movie::statusOf(string s){
	if(s=="Out"){
		return Movie::Status::Out;
	}
	else if(s=="Discontinued"){
		return Movie::Status::Discontinued;
	}
	else {
		return Movie::Status::Cancelled;
	}
}
Movie::Genres Movie::genreOf(string g){
	/*
	(1) Suspense   (2) Foreign   (3) Comedy    (4) Western   (5) Music   (6) Drama 
	*/
	if (g.find("Suspense") != string::npos) {
		return Movie::Genres::Suspense;
	}
	else if(g.find("Foreign") != string::npos){
		return Movie::Genres::Foreign;
	}
	else if(g.find("Comedy") != string::npos){
		return Movie::Genres::Comedy;
	}
	else if(g.find("Western") != string::npos){
		return Movie::Genres::Western;
	}
	else if(g.find("Music") != string::npos){
		return Movie::Genres::Music;
	}
	else if(g.find("Drama") != string::npos){
		return Movie::Genres::Drama;
	}
	else{
		return Movie::Genres::Other;
	}
}