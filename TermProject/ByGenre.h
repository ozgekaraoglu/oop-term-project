#pragma once
#include "Predicate.h"
#include "Movie.h"
class ByGenre : public Predicate
{
public:
	static ByGenre *suspense;
	static ByGenre* comedy;
	static ByGenre* drama;
	static ByGenre* foreign;
	static ByGenre* music;
	static ByGenre* western;
	static ByGenre* other;

	ByGenre(Movie::Genres genre);
	virtual bool test(Movie* movie);
	~ByGenre(void);
private:
	Movie::Genres _genre;
};

