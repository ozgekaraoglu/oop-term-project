//
//  ByID.hpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/4/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#ifndef ByID_hpp
#define ByID_hpp

#include "Predicate.h"
#include "Movie.h"
class ByID : public Predicate
{
public:
    static Predicate* of(string);
    virtual bool test(Movie* movie);
    ~ByID(void);
private:
    ByID(string);
    string _id;
};
#endif /* ByID_hpp */
