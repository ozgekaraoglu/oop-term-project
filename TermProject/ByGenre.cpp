#include "ByGenre.h"

 ByGenre* ByGenre::suspense = new ByGenre(Movie::Genres::Suspense);
 ByGenre* ByGenre::comedy = new ByGenre(Movie::Genres::Comedy);
 ByGenre* ByGenre::drama = new ByGenre(Movie::Genres::Drama);
 ByGenre* ByGenre::foreign = new ByGenre(Movie::Genres::Foreign);
 ByGenre* ByGenre::music = new ByGenre(Movie::Genres::Music);
 ByGenre* ByGenre::western = new ByGenre(Movie::Genres::Western);
 ByGenre* ByGenre::other = new ByGenre(Movie::Genres::Other);

ByGenre::ByGenre(Movie::Genres genre): _genre(genre)
{
}
bool ByGenre::test(Movie* movie){
	return movie->getGenre() == _genre;
}

ByGenre::~ByGenre(void)
{
}
