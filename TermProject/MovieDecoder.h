#pragma once
#include "FileLineAcceptor.h"
#include "Movie.h"
#include <vector>
using namespace std;

//Dosyadaki her bir line'� consume eden implementasyon
class MovieDecoder: public FileLineAcceptor
{
public:
	MovieDecoder(void);
	virtual void startOfFile();
	virtual void accept(string line);
	virtual void endOfFile();
	vector<Movie*> getMovies();
	~MovieDecoder(void);
private:
	Movie* decode(string line);
	vector<Movie*> movies;
};

