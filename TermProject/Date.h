#ifndef DATE_H
#define DATE_H
#include <string>
using namespace std;

class Date{
	
	private:
		int _day;
		int _month;
		int _year;

	public:
		Date(string date);
		Date(int month, int day,int year);
        int getDay() const {return _day;}
        int getMonth() const {return _month;}
        int getYear() const {return _year;}
        bool operator>(Date* date);
        ~Date(void);

};


#endif