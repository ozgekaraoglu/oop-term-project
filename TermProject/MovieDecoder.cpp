#include "MovieDecoder.h"
#include "Util.h"

Date *emptyDate = new Date(-1,-1,-1);

MovieDecoder::MovieDecoder(void)
{
}
void MovieDecoder::startOfFile(){
	movies.reserve(10000);
	movies.clear();
}
void MovieDecoder::accept(string line){
	movies.push_back(decode(line));
}
void MovieDecoder::endOfFile(){
}
Movie* MovieDecoder::decode(string m){
	vector<string> e;
    Util::split(m, '\t', e);
	Date *date;
	if(e.at(5)==""){
		date = emptyDate;
	}else{
		date = new Date(e.at(5));
	}
	return new Movie(e.at(0),e.at(1),e.at(2),e.at(3),e.at(4), date, e.at(6));
}

vector<Movie*> MovieDecoder::getMovies(){
	return movies;
}
MovieDecoder::~MovieDecoder(void)
{
}
