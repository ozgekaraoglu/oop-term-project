#pragma once
#include "Movie.h"
#include "Predicate.h"
#include "MovieConsumer.h"
#include <vector>
using namespace std;
class MovieController
{
public:
    MovieController(vector<Movie*>::iterator begin, vector<Movie*>::iterator end);
    ~MovieController(void);
	MovieController* filter(Predicate* predicate);
	static double compareByPrice(Movie *first, Movie *second);
	double average();
	int count();
    void forEach(MovieConsumer* consumer);
    Movie* findFirst();
	
	
private:
    MovieController(MovieConsumer *consumer, MovieController* head,vector<Movie*>::iterator begin, vector<Movie*>::iterator end);
    MovieController* _head;
	MovieConsumer* downstream;
	vector<Movie*>::iterator _begin;
	vector<Movie*>::iterator _end;
};

