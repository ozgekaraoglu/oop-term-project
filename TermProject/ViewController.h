#pragma once
#include "MovieController.h"
#include "MovieConsumer.h"
#include "Display.h"
#include <string>
using namespace std;

class ViewController
{
public:
	ViewController(MovieController* controller);
	~ViewController(void);
	void displayMenu();
private:
    MovieController* _controller;
    Display* _display;
    void searchAndDisplayMovieBy();
    void searchMovieByDVDTitle(string title);
    void searchMovieByID(string title);
    void compareMoviesBasedOnTheirPriceInfo();
    void listMoviesBasedOnGenres();
    void displayStatistics();
    void displayMovieAfterInputDate();
    Movie* findById();
};

