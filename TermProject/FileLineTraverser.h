#pragma once
#include<iostream>
#include<fstream>
#include<string>
#include "FileLineAcceptor.h"
using namespace std;
class FileLineTraverser
{

	public:
		FileLineTraverser(string path): _path(path){};
		~FileLineTraverser(void){};
		void traverseUsing(FileLineAcceptor* acceptor)
		{
			string line;
			ifstream myfile(_path);
			if (myfile.is_open())
			{
				acceptor->startOfFile();
				while(getline (myfile,line)){
					acceptor->accept(line);
				}
				acceptor->endOfFile();
				myfile.close();
			}else{
				cout << "Unable to open file"; 
			}
		}
	private:
		string _path;	
};

