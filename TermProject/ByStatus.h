#pragma once
#include "Predicate.h"
#include "Movie.h"
class ByStatus :
	public Predicate
{
public:

	static ByStatus *Discontinued;
	static ByStatus *Out;
	static ByStatus *Cancelled;

	ByStatus(Movie::Status status);
	virtual bool test(Movie* movie);
	~ByStatus(void);
private:
	Movie::Status _status;
};

