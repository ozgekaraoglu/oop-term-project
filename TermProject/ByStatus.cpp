#include "ByStatus.h"

 ByStatus* ByStatus::Discontinued = new ByStatus(Movie::Status::Discontinued);
 ByStatus* ByStatus::Out = new ByStatus(Movie::Status::Out);
 ByStatus* ByStatus::Cancelled = new ByStatus(Movie::Status::Cancelled);

ByStatus::ByStatus(Movie::Status status) : _status(status)
{
}
bool ByStatus::test(Movie* movie){
	return movie->getStatus() == _status;
}

ByStatus::~ByStatus(void)
{
}
