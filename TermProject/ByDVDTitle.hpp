//
//  ByDVDTitle.hpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/4/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#ifndef ByDVDTitle_hpp
#define ByDVDTitle_hpp

#include <stdio.h>
#include "Predicate.h"
class ByDVDTitle : public Predicate{
    
public:
    static Predicate* of(string);
    virtual bool test(Movie* movie);
    ~ByDVDTitle();
    
    
private:
    ByDVDTitle(string);
    string _DVDTitle;
    
};
#endif /* ByDVDTitle_hpp */
