#pragma once
#include "MovieConsumer.h"
class Display : public MovieConsumer
{
public:
	Display(void);
	~Display(void);
	virtual void begin();
	virtual void accept(Movie* movie);
	virtual void end();
    
};

