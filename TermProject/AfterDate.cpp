//
//  AfterDate.cpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/3/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#include "AfterDate.hpp"
AfterDate::AfterDate(Date* date) : _date(date){
    
}

bool AfterDate::test(Movie* movie){
    return _date > movie->getDVD_ReleaseDate();
}

AfterDate::~AfterDate(){
    
}