#pragma once
#include<string>
using namespace std;

//dosyadaki sat�rlar� kabul eden protocol/interface
class FileLineAcceptor
{
public:
	virtual void startOfFile() = 0;
	virtual void accept(string line)=0;
	virtual void endOfFile() = 0;
};

