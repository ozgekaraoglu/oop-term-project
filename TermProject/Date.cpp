#include"Date.h"
#include<iostream>
#include<string>
#include "Util.h"
using namespace std;


Date::Date(int month, int day,int year):_day(day),_month(month),_year(year){}

Date::Date(string date){
	vector<string> e;
	Util::split(date, '/', e);
	
	Date(stoi(e.at(0)),stoi(e.at(1)),stoi(e.at(2)));
}
bool Date::operator>(Date* date){
    return _year > date->getYear() && _month > date->getMonth() && _day > date->getDay();
}
Date::~Date(void){}