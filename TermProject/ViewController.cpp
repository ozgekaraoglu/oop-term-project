#include <iostream>
#include "ViewController.h"
#include "ByDVDTitle.hpp"
#include "ByID.hpp"
#include "ByGenre.h"
#include "ByStatus.h"
#include "GreaterThanPrice.hpp"
#include "AfterDate.hpp"

ViewController::ViewController(MovieController* controller):_controller(controller)
{
    _display = new Display();
}


ViewController::~ViewController(void)
{
}

void ViewController::displayMenu(){
	cout << "(1) Search and Display a movie by:" << endl;
	cout << "(2) Compare two movies based on their price information" << endl;
	cout << "(3) List movies based on following genres " << endl;
	cout << "(4) Display Statistics" << endl;
	cout << "(5) Display movies after an input date" << endl;
	cout << "(0) Exit" << endl;
	int n;
	cout << "Please enter a number by menu: (0,1,2,3,4,5) " << endl;
	cin >> n;
	switch(n)
	{
	case 0:
            break;
	case 1:
            searchAndDisplayMovieBy();
            displayMenu();
            break;
	case 2:
            compareMoviesBasedOnTheirPriceInfo();
            displayMenu();
            break;
	case 3:
            listMoviesBasedOnGenres();
            displayMenu();
            break;
	case 4:
            displayStatistics();
            displayMenu();
            break;
	case 5:
            displayMovieAfterInputDate();
            displayMenu();
            break;
    default:
        displayMenu();
        break;
	}

}
void ViewController::searchAndDisplayMovieBy(){
    int n;
    cout << "(1)DVD_Title " << endl;
    cout << "(2)ID" << endl;
    cout << "Please enter a number: (1,2) " << endl;
    cin >> n;
    string in;
    switch (n) {
        case 1:
            cout << "Please enter DVD_Title: " << endl;
            cin.ignore();
            getline( cin, in );
            searchMovieByDVDTitle(in);
            break;
        case 2:
            cout << "Please enter ID: " << endl;
            cin.ignore();
            getline( cin, in );
            searchMovieByID(in);
            break;
        default:
            searchAndDisplayMovieBy();
            break;
    }
}
void ViewController::searchMovieByDVDTitle(string title){
    _controller->filter(ByDVDTitle::of(title))->forEach(_display);
}
void ViewController::searchMovieByID(string id){
    _controller->filter(ByID::of(id))->forEach(_display);
}
Movie* ViewController::findById(){
    string id;
    cout << "Enter ID " << endl;
    cin >>id;
    cin.clear();
    return _controller->filter(ByID::of(id))->findFirst();
}
void ViewController::compareMoviesBasedOnTheirPriceInfo(){
    Movie* first = findById();
    if(nullptr==first){
        cout << "There is no movie with such an ID" << endl;
        return;
    }
    Movie* second = findById();
    if(nullptr==second){
        cout << "There is no movie with such an ID" << endl;
        return;
    }
    
    if(second>first){
        cout << *second << " is Expensive Than " << *first << endl;
    }else if(second == first){
        cout << *second << " is Equal To " << *first << endl;
    }else{
        cout << *first << " is Expensive Than " << *second << endl;
    }
    
}
void ViewController::listMoviesBasedOnGenres(){
    int n;
    cout << "(1) Suspense" << endl <<  "(2) Foreign" << endl << "(3) Comedy" << endl <<  "(4) Western" << endl << "(5) Music" << endl << "(6) Drama" << endl;
    cout << "Please enter a number: "<< endl;
    cin >> n;
    
    string in;
    switch (n) {
        case 1:
            _controller->filter(ByGenre::suspense)->forEach(_display);
            break;
        case 2:
            _controller->filter(ByGenre::foreign)->forEach(_display);
            break;
        case 3:
            _controller->filter(ByGenre::comedy)->forEach(_display);
            break;
        case 4:
             _controller->filter(ByGenre::western)->forEach(_display);
            break;
        case 5:
            _controller->filter(ByGenre::music)->forEach(_display);
            break;
        case 6:
            _controller->filter(ByGenre::drama)->forEach(_display);
            break;
        default:
            listMoviesBasedOnGenres();
            break;
    }
}
void ViewController::displayStatistics(){
    cout << "(1) Display number of each genres" << endl << "(2) Display average price" << endl << "(3) Display the number of movies whose price is greater than an input"<< endl;
    cout << "(4) Display average price of ''Discontinued'' movies" << "(5) Display average price of ''Out'' movies" << endl <<	"(6) Display average price of ''Cancelled'' movies" << endl;
    cout << "Please enter a number: "<< endl;
    int n;
    cin >> n;
    
    switch (n) {
        case 1:
            cout << "Count of Suspense " << _controller->filter(ByGenre::suspense)->count() << endl;
            cout << "Count of Foreign " << _controller->filter(ByGenre::foreign)->count() << endl;
            cout << "Count of Comedy " << _controller->filter(ByGenre::comedy)->count() << endl;
            cout << "Count of Western " << _controller->filter(ByGenre::western)->count() << endl;
            cout << "Count of Music " << _controller->filter(ByGenre::music)->count() << endl;
            cout << "Count of Drama " << _controller->filter(ByGenre::drama)->count() << endl;
            break;
        case 2:
            cout << "Average Price of All " << _controller->average() << endl;
            break;
        case 3:{
            double price;
            cout << "Please enter the price: " << endl;
            cin.ignore();
            cin >> price;
            GreaterThanPrice *greaterThanPrice = new GreaterThanPrice(price);
            cout << "Number of movies whose price is greater than " << price << " is " << _controller->filter(greaterThanPrice)->count() << endl;
            break;
        }
        case 4:
            cout << "Average Price of Discontinued Movies => " << _controller->filter(ByStatus::Discontinued)->average() << endl;
            break;
        case 5:
            cout << "Average Price of Out Movies => " << _controller->filter(ByStatus::Out)->average() << endl;
            break;
        case 6:
            cout << "Average Price of Cancelled Movies => " << _controller->filter(ByStatus::Cancelled)->average() << endl;
            break;
        default:
            displayStatistics();
            break;
    }
    
}
void ViewController::displayMovieAfterInputDate(){
    string in;
    cout << "Please enter input date: "<< endl;
    getline( cin, in );
    Date *inDate = new Date(in);
    _controller->filter(new AfterDate(inDate))->forEach(_display);
}