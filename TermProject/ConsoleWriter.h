#pragma once
#include "FileLineAcceptor.h"
#include <iostream>
using namespace std;

//Dosyadaki her bir line'� konsola basarak consume eden implementasyon
class ConsoleWriter: public FileLineAcceptor
{
public:
	ConsoleWriter(void){}
	virtual void startOfFile(){
		cout << "START OF FILE" << endl;
	}
	virtual void accept(string line){
		cout << line << endl;
	}
	virtual void endOfFile(){
		cout << "END OF FILE" << endl;
	}
	~ConsoleWriter(void){}
};

