#include "Util.h"
#include <sstream>

Util::Util(void)
{
}


Util::~Util(void)
{
}

vector<std::string>& Util::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

string Util::replaceStar(string str){
    if(str.find("*")!=std::string::npos){
        return str.replace(str.find("*"), sizeof("*")-1, "");
    }else{
        return str;
    }
}