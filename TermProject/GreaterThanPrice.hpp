//
//  ByPrice.hpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/3/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#ifndef ByPrice_hpp
#define ByPrice_hpp

#include "Predicate.h"
#include "Movie.h"
class GreaterThanPrice : public Predicate {
public:
    GreaterThanPrice(double);
    virtual bool test(Movie* movie);
    ~GreaterThanPrice();
    
private:
    double _Price;
};
#endif /* ByPrice_hpp */
