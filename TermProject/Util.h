#pragma once
#include <vector>
#include <string>

using namespace std;
class Util
{
public:
	Util(void);
	static vector<std::string>& split(const string &s, char delim, vector<string> &elems);
    static string replaceStar(string);
	~Util(void);
};

