#pragma once
#include "Movie.h"
class Predicate
{
public:
	virtual bool test(Movie* movie) = 0;
};

