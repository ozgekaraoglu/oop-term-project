//
//  ByID.cpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/4/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#include "ByID.hpp"
#include "Util.h"
ByID::ByID(string id): _id(id){}
Predicate* ByID::of(string id){
    if (id.find("*") != std::string::npos){
        class ByIDWithStar : public  Predicate{
        public:
            ByIDWithStar(string id) : _id(Util::replaceStar(id)){}
            ~ByIDWithStar(){}
            
            virtual bool test(Movie* movie){
                return movie->getID().compare(0,_id.length(),_id)==0;
            }
        private:
            string _id;
            
        };
        return new ByIDWithStar(id);
    }else {
        return new ByID(id);
    }
}

bool ByID::test(Movie* movie){
    return movie->getID()==_id;
}

ByID::~ByID(void){}
