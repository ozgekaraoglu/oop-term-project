//
//  GreaterThanPrice.cpp
//  TermProject
//
//  Created by Sercan Karaoglu on 5/3/16.
//  Copyright © 2016 Sercan Karaoglu. All rights reserved.
//

#include "GreaterThanPrice.hpp"

GreaterThanPrice::GreaterThanPrice(double Price) : _Price(Price){
    
}

bool GreaterThanPrice::test(Movie* movie){
    return _Price > movie->getPrice();
}


GreaterThanPrice::~GreaterThanPrice(){}